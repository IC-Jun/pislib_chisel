# A Library for PIM-In-SoC [PIS Lib] -- Chisel Version



PIM: Processing-In-Memory; SoC: System-on-a-Chip

### For full documentation visit [https://bonany.gitlab.io/pis/](https://bonany.gitlab.io/pis/)

### Author: Bonan Yan

---

## Source codes

### Behavioral model:
```
/src/main/scala/pislib/*
```

| File          | Content                               |
| - | - | 
| Memory.scala  | traditional memory behavioral model   |
| APIM.scala    | analog PIM macro behavioral model     |  
| DPIM.scala    | digital PIM macro behavioral model    |

### Chisel testbench:
```
/src/test/scala/pislib/*
```

---

## Main entry:

### First enter the working folder (where the "makefile" lies)

```
cd [project_folder]
```

Select one of the following:

### (a) Test normal memory:
```
make testMem
```
*Explain*: test conventional memory chisel behavioral model

### (b) Test analog PIM:
```
make testAPIM 
```
*Explain*: test analog PIM macro chisel behavioral model

### (c) Test digital PIM:
```
make testDPIM 
```
*Explain*: test digital PIM macro chisel behavioral model

---
## Run environment setup:
Refer to [chisel local install]
- install *Java SDK* 
- install *sbt*
- Good to go run the main entry

---
copyright to $\copyright$ Bonan Yan, 2022 