
default: testAPIM

testMem:
	sbt -v "testOnly pislib.MemTest"

testDPIM:
	sbt -v "testOnly pislib.DPIMTester"

testAPIM:
	sbt -v "testOnly pislib.APIMTester"

verilogMem:
	sbt -v "runMain pislib.VerilogGen_Mem"

verilogDPIM:
	sbt -v "runMain pislib.VerilogGen_DPIM"

verilogAPIM:
	sbt -v "runMain pislib.VerilogGen_APIM"

clean:
	rm -rf generated/*