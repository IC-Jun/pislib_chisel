module APIM(
  input        clock,
  input        reset,
  input  [9:0] io_addr,
  input  [7:0] io_d,
  output [7:0] io_q,
  input        io_we,
  input        io_cme,
  input  [3:0] io_cmIn_0,
  input  [3:0] io_cmIn_1,
  input  [3:0] io_cmIn_2,
  input  [3:0] io_cmIn_3,
  input  [3:0] io_cmIn_4,
  input  [3:0] io_cmIn_5,
  input  [3:0] io_cmIn_6,
  input  [3:0] io_cmIn_7,
  output [3:0] io_cmOut_0,
  output [3:0] io_cmOut_1,
  output [3:0] io_cmOut_2,
  output [3:0] io_cmOut_3,
  output [3:0] io_cmOut_4,
  output [3:0] io_cmOut_5,
  output [3:0] io_cmOut_6,
  output [3:0] io_cmOut_7
);
`ifdef RANDOMIZE_MEM_INIT
  reg [31:0] _RAND_0;
`endif // RANDOMIZE_MEM_INIT
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
  reg [31:0] _RAND_67;
  reg [31:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [31:0] _RAND_71;
  reg [31:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [31:0] _RAND_74;
`endif // RANDOMIZE_REG_INIT
  reg [7:0] mem [0:1023]; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_0_MPORT_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_0_MPORT_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_0_MPORT_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_0_MPORT_1_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_0_MPORT_1_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_0_MPORT_1_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_0_MPORT_2_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_0_MPORT_2_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_0_MPORT_2_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_0_MPORT_3_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_0_MPORT_3_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_0_MPORT_3_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_1_MPORT_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_1_MPORT_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_1_MPORT_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_1_MPORT_1_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_1_MPORT_1_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_1_MPORT_1_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_1_MPORT_2_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_1_MPORT_2_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_1_MPORT_2_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_1_MPORT_3_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_1_MPORT_3_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_1_MPORT_3_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_2_MPORT_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_2_MPORT_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_2_MPORT_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_2_MPORT_1_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_2_MPORT_1_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_2_MPORT_1_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_2_MPORT_2_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_2_MPORT_2_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_2_MPORT_2_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_2_MPORT_3_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_2_MPORT_3_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_2_MPORT_3_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_3_MPORT_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_3_MPORT_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_3_MPORT_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_3_MPORT_1_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_3_MPORT_1_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_3_MPORT_1_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_3_MPORT_2_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_3_MPORT_2_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_3_MPORT_2_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_3_MPORT_3_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_3_MPORT_3_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_3_MPORT_3_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_4_MPORT_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_4_MPORT_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_4_MPORT_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_4_MPORT_1_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_4_MPORT_1_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_4_MPORT_1_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_4_MPORT_2_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_4_MPORT_2_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_4_MPORT_2_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_4_MPORT_3_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_4_MPORT_3_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_4_MPORT_3_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_5_MPORT_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_5_MPORT_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_5_MPORT_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_5_MPORT_1_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_5_MPORT_1_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_5_MPORT_1_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_5_MPORT_2_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_5_MPORT_2_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_5_MPORT_2_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_5_MPORT_3_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_5_MPORT_3_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_5_MPORT_3_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_6_MPORT_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_6_MPORT_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_6_MPORT_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_6_MPORT_1_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_6_MPORT_1_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_6_MPORT_1_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_6_MPORT_2_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_6_MPORT_2_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_6_MPORT_2_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_6_MPORT_3_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_6_MPORT_3_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_6_MPORT_3_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_7_MPORT_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_7_MPORT_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_7_MPORT_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_7_MPORT_1_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_7_MPORT_1_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_7_MPORT_1_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_7_MPORT_2_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_7_MPORT_2_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_7_MPORT_2_data; // @[APIM.scala 37:24]
  wire  mem_cmOutTmp_7_MPORT_3_en; // @[APIM.scala 37:24]
  wire [9:0] mem_cmOutTmp_7_MPORT_3_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_cmOutTmp_7_MPORT_3_data; // @[APIM.scala 37:24]
  wire  mem_io_q_MPORT_en; // @[APIM.scala 37:24]
  wire [9:0] mem_io_q_MPORT_addr; // @[APIM.scala 37:24]
  wire [7:0] mem_io_q_MPORT_data; // @[APIM.scala 37:24]
  wire [7:0] mem_MPORT_data; // @[APIM.scala 37:24]
  wire [9:0] mem_MPORT_addr; // @[APIM.scala 37:24]
  wire  mem_MPORT_mask; // @[APIM.scala 37:24]
  wire  mem_MPORT_en; // @[APIM.scala 37:24]
  reg  mem_cmOutTmp_0_MPORT_en_pipe_0;
  reg [9:0] mem_cmOutTmp_0_MPORT_addr_pipe_0;
  reg  mem_cmOutTmp_0_MPORT_1_en_pipe_0;
  reg [9:0] mem_cmOutTmp_0_MPORT_1_addr_pipe_0;
  reg  mem_cmOutTmp_0_MPORT_2_en_pipe_0;
  reg [9:0] mem_cmOutTmp_0_MPORT_2_addr_pipe_0;
  reg  mem_cmOutTmp_0_MPORT_3_en_pipe_0;
  reg [9:0] mem_cmOutTmp_0_MPORT_3_addr_pipe_0;
  reg  mem_cmOutTmp_1_MPORT_en_pipe_0;
  reg [9:0] mem_cmOutTmp_1_MPORT_addr_pipe_0;
  reg  mem_cmOutTmp_1_MPORT_1_en_pipe_0;
  reg [9:0] mem_cmOutTmp_1_MPORT_1_addr_pipe_0;
  reg  mem_cmOutTmp_1_MPORT_2_en_pipe_0;
  reg [9:0] mem_cmOutTmp_1_MPORT_2_addr_pipe_0;
  reg  mem_cmOutTmp_1_MPORT_3_en_pipe_0;
  reg [9:0] mem_cmOutTmp_1_MPORT_3_addr_pipe_0;
  reg  mem_cmOutTmp_2_MPORT_en_pipe_0;
  reg [9:0] mem_cmOutTmp_2_MPORT_addr_pipe_0;
  reg  mem_cmOutTmp_2_MPORT_1_en_pipe_0;
  reg [9:0] mem_cmOutTmp_2_MPORT_1_addr_pipe_0;
  reg  mem_cmOutTmp_2_MPORT_2_en_pipe_0;
  reg [9:0] mem_cmOutTmp_2_MPORT_2_addr_pipe_0;
  reg  mem_cmOutTmp_2_MPORT_3_en_pipe_0;
  reg [9:0] mem_cmOutTmp_2_MPORT_3_addr_pipe_0;
  reg  mem_cmOutTmp_3_MPORT_en_pipe_0;
  reg [9:0] mem_cmOutTmp_3_MPORT_addr_pipe_0;
  reg  mem_cmOutTmp_3_MPORT_1_en_pipe_0;
  reg [9:0] mem_cmOutTmp_3_MPORT_1_addr_pipe_0;
  reg  mem_cmOutTmp_3_MPORT_2_en_pipe_0;
  reg [9:0] mem_cmOutTmp_3_MPORT_2_addr_pipe_0;
  reg  mem_cmOutTmp_3_MPORT_3_en_pipe_0;
  reg [9:0] mem_cmOutTmp_3_MPORT_3_addr_pipe_0;
  reg  mem_cmOutTmp_4_MPORT_en_pipe_0;
  reg [9:0] mem_cmOutTmp_4_MPORT_addr_pipe_0;
  reg  mem_cmOutTmp_4_MPORT_1_en_pipe_0;
  reg [9:0] mem_cmOutTmp_4_MPORT_1_addr_pipe_0;
  reg  mem_cmOutTmp_4_MPORT_2_en_pipe_0;
  reg [9:0] mem_cmOutTmp_4_MPORT_2_addr_pipe_0;
  reg  mem_cmOutTmp_4_MPORT_3_en_pipe_0;
  reg [9:0] mem_cmOutTmp_4_MPORT_3_addr_pipe_0;
  reg  mem_cmOutTmp_5_MPORT_en_pipe_0;
  reg [9:0] mem_cmOutTmp_5_MPORT_addr_pipe_0;
  reg  mem_cmOutTmp_5_MPORT_1_en_pipe_0;
  reg [9:0] mem_cmOutTmp_5_MPORT_1_addr_pipe_0;
  reg  mem_cmOutTmp_5_MPORT_2_en_pipe_0;
  reg [9:0] mem_cmOutTmp_5_MPORT_2_addr_pipe_0;
  reg  mem_cmOutTmp_5_MPORT_3_en_pipe_0;
  reg [9:0] mem_cmOutTmp_5_MPORT_3_addr_pipe_0;
  reg  mem_cmOutTmp_6_MPORT_en_pipe_0;
  reg [9:0] mem_cmOutTmp_6_MPORT_addr_pipe_0;
  reg  mem_cmOutTmp_6_MPORT_1_en_pipe_0;
  reg [9:0] mem_cmOutTmp_6_MPORT_1_addr_pipe_0;
  reg  mem_cmOutTmp_6_MPORT_2_en_pipe_0;
  reg [9:0] mem_cmOutTmp_6_MPORT_2_addr_pipe_0;
  reg  mem_cmOutTmp_6_MPORT_3_en_pipe_0;
  reg [9:0] mem_cmOutTmp_6_MPORT_3_addr_pipe_0;
  reg  mem_cmOutTmp_7_MPORT_en_pipe_0;
  reg [9:0] mem_cmOutTmp_7_MPORT_addr_pipe_0;
  reg  mem_cmOutTmp_7_MPORT_1_en_pipe_0;
  reg [9:0] mem_cmOutTmp_7_MPORT_1_addr_pipe_0;
  reg  mem_cmOutTmp_7_MPORT_2_en_pipe_0;
  reg [9:0] mem_cmOutTmp_7_MPORT_2_addr_pipe_0;
  reg  mem_cmOutTmp_7_MPORT_3_en_pipe_0;
  reg [9:0] mem_cmOutTmp_7_MPORT_3_addr_pipe_0;
  reg  mem_io_q_MPORT_en_pipe_0;
  reg [9:0] mem_io_q_MPORT_addr_pipe_0;
  reg [31:0] cmOutTmp_0; // @[APIM.scala 40:21]
  reg [31:0] cmOutTmp_1; // @[APIM.scala 40:21]
  reg [31:0] cmOutTmp_2; // @[APIM.scala 40:21]
  reg [31:0] cmOutTmp_3; // @[APIM.scala 40:21]
  reg [31:0] cmOutTmp_4; // @[APIM.scala 40:21]
  reg [31:0] cmOutTmp_5; // @[APIM.scala 40:21]
  reg [31:0] cmOutTmp_6; // @[APIM.scala 40:21]
  reg [31:0] cmOutTmp_7; // @[APIM.scala 40:21]
  wire [9:0] _cmOutTmp_0_T_3 = io_addr & 10'h1c7; // @[APIM.scala 44:72]
  wire [11:0] _cmOutTmp_0_T_7 = io_cmIn_0 * mem_cmOutTmp_0_MPORT_data; // @[APIM.scala 44:22]
  wire [11:0] _cmOutTmp_0_T_15 = io_cmIn_1 * mem_cmOutTmp_0_MPORT_1_data; // @[APIM.scala 45:22]
  wire [11:0] _cmOutTmp_0_T_17 = _cmOutTmp_0_T_7 + _cmOutTmp_0_T_15; // @[APIM.scala 44:96]
  wire [11:0] _cmOutTmp_0_T_25 = io_cmIn_2 * mem_cmOutTmp_0_MPORT_2_data; // @[APIM.scala 46:22]
  wire [11:0] _cmOutTmp_0_T_27 = _cmOutTmp_0_T_17 + _cmOutTmp_0_T_25; // @[APIM.scala 45:96]
  wire [11:0] _cmOutTmp_0_T_35 = io_cmIn_3 * mem_cmOutTmp_0_MPORT_3_data; // @[APIM.scala 47:22]
  wire [11:0] _cmOutTmp_0_T_37 = _cmOutTmp_0_T_27 + _cmOutTmp_0_T_35; // @[APIM.scala 46:96]
  wire [11:0] _GEN_16 = io_cme ? _cmOutTmp_0_T_37 : 12'h0; // @[APIM.scala 42:18 43:19 49:19]
  wire [11:0] _cmOutTmp_1_T_7 = io_cmIn_0 * mem_cmOutTmp_1_MPORT_data; // @[APIM.scala 44:22]
  wire [11:0] _cmOutTmp_1_T_15 = io_cmIn_1 * mem_cmOutTmp_1_MPORT_1_data; // @[APIM.scala 45:22]
  wire [11:0] _cmOutTmp_1_T_17 = _cmOutTmp_1_T_7 + _cmOutTmp_1_T_15; // @[APIM.scala 44:96]
  wire [11:0] _cmOutTmp_1_T_25 = io_cmIn_2 * mem_cmOutTmp_1_MPORT_2_data; // @[APIM.scala 46:22]
  wire [11:0] _cmOutTmp_1_T_27 = _cmOutTmp_1_T_17 + _cmOutTmp_1_T_25; // @[APIM.scala 45:96]
  wire [11:0] _cmOutTmp_1_T_35 = io_cmIn_3 * mem_cmOutTmp_1_MPORT_3_data; // @[APIM.scala 47:22]
  wire [11:0] _cmOutTmp_1_T_37 = _cmOutTmp_1_T_27 + _cmOutTmp_1_T_35; // @[APIM.scala 46:96]
  wire [11:0] _GEN_29 = io_cme ? _cmOutTmp_1_T_37 : 12'h0; // @[APIM.scala 42:18 43:19 49:19]
  wire [11:0] _cmOutTmp_2_T_7 = io_cmIn_0 * mem_cmOutTmp_2_MPORT_data; // @[APIM.scala 44:22]
  wire [11:0] _cmOutTmp_2_T_15 = io_cmIn_1 * mem_cmOutTmp_2_MPORT_1_data; // @[APIM.scala 45:22]
  wire [11:0] _cmOutTmp_2_T_17 = _cmOutTmp_2_T_7 + _cmOutTmp_2_T_15; // @[APIM.scala 44:96]
  wire [11:0] _cmOutTmp_2_T_25 = io_cmIn_2 * mem_cmOutTmp_2_MPORT_2_data; // @[APIM.scala 46:22]
  wire [11:0] _cmOutTmp_2_T_27 = _cmOutTmp_2_T_17 + _cmOutTmp_2_T_25; // @[APIM.scala 45:96]
  wire [11:0] _cmOutTmp_2_T_35 = io_cmIn_3 * mem_cmOutTmp_2_MPORT_3_data; // @[APIM.scala 47:22]
  wire [11:0] _cmOutTmp_2_T_37 = _cmOutTmp_2_T_27 + _cmOutTmp_2_T_35; // @[APIM.scala 46:96]
  wire [11:0] _GEN_42 = io_cme ? _cmOutTmp_2_T_37 : 12'h0; // @[APIM.scala 42:18 43:19 49:19]
  wire [11:0] _cmOutTmp_3_T_7 = io_cmIn_0 * mem_cmOutTmp_3_MPORT_data; // @[APIM.scala 44:22]
  wire [11:0] _cmOutTmp_3_T_15 = io_cmIn_1 * mem_cmOutTmp_3_MPORT_1_data; // @[APIM.scala 45:22]
  wire [11:0] _cmOutTmp_3_T_17 = _cmOutTmp_3_T_7 + _cmOutTmp_3_T_15; // @[APIM.scala 44:96]
  wire [11:0] _cmOutTmp_3_T_25 = io_cmIn_2 * mem_cmOutTmp_3_MPORT_2_data; // @[APIM.scala 46:22]
  wire [11:0] _cmOutTmp_3_T_27 = _cmOutTmp_3_T_17 + _cmOutTmp_3_T_25; // @[APIM.scala 45:96]
  wire [11:0] _cmOutTmp_3_T_35 = io_cmIn_3 * mem_cmOutTmp_3_MPORT_3_data; // @[APIM.scala 47:22]
  wire [11:0] _cmOutTmp_3_T_37 = _cmOutTmp_3_T_27 + _cmOutTmp_3_T_35; // @[APIM.scala 46:96]
  wire [11:0] _GEN_55 = io_cme ? _cmOutTmp_3_T_37 : 12'h0; // @[APIM.scala 42:18 43:19 49:19]
  wire [11:0] _cmOutTmp_4_T_7 = io_cmIn_0 * mem_cmOutTmp_4_MPORT_data; // @[APIM.scala 44:22]
  wire [11:0] _cmOutTmp_4_T_15 = io_cmIn_1 * mem_cmOutTmp_4_MPORT_1_data; // @[APIM.scala 45:22]
  wire [11:0] _cmOutTmp_4_T_17 = _cmOutTmp_4_T_7 + _cmOutTmp_4_T_15; // @[APIM.scala 44:96]
  wire [11:0] _cmOutTmp_4_T_25 = io_cmIn_2 * mem_cmOutTmp_4_MPORT_2_data; // @[APIM.scala 46:22]
  wire [11:0] _cmOutTmp_4_T_27 = _cmOutTmp_4_T_17 + _cmOutTmp_4_T_25; // @[APIM.scala 45:96]
  wire [11:0] _cmOutTmp_4_T_35 = io_cmIn_3 * mem_cmOutTmp_4_MPORT_3_data; // @[APIM.scala 47:22]
  wire [11:0] _cmOutTmp_4_T_37 = _cmOutTmp_4_T_27 + _cmOutTmp_4_T_35; // @[APIM.scala 46:96]
  wire [11:0] _GEN_68 = io_cme ? _cmOutTmp_4_T_37 : 12'h0; // @[APIM.scala 42:18 43:19 49:19]
  wire [11:0] _cmOutTmp_5_T_7 = io_cmIn_0 * mem_cmOutTmp_5_MPORT_data; // @[APIM.scala 44:22]
  wire [11:0] _cmOutTmp_5_T_15 = io_cmIn_1 * mem_cmOutTmp_5_MPORT_1_data; // @[APIM.scala 45:22]
  wire [11:0] _cmOutTmp_5_T_17 = _cmOutTmp_5_T_7 + _cmOutTmp_5_T_15; // @[APIM.scala 44:96]
  wire [11:0] _cmOutTmp_5_T_25 = io_cmIn_2 * mem_cmOutTmp_5_MPORT_2_data; // @[APIM.scala 46:22]
  wire [11:0] _cmOutTmp_5_T_27 = _cmOutTmp_5_T_17 + _cmOutTmp_5_T_25; // @[APIM.scala 45:96]
  wire [11:0] _cmOutTmp_5_T_35 = io_cmIn_3 * mem_cmOutTmp_5_MPORT_3_data; // @[APIM.scala 47:22]
  wire [11:0] _cmOutTmp_5_T_37 = _cmOutTmp_5_T_27 + _cmOutTmp_5_T_35; // @[APIM.scala 46:96]
  wire [11:0] _GEN_81 = io_cme ? _cmOutTmp_5_T_37 : 12'h0; // @[APIM.scala 42:18 43:19 49:19]
  wire [11:0] _cmOutTmp_6_T_7 = io_cmIn_0 * mem_cmOutTmp_6_MPORT_data; // @[APIM.scala 44:22]
  wire [11:0] _cmOutTmp_6_T_15 = io_cmIn_1 * mem_cmOutTmp_6_MPORT_1_data; // @[APIM.scala 45:22]
  wire [11:0] _cmOutTmp_6_T_17 = _cmOutTmp_6_T_7 + _cmOutTmp_6_T_15; // @[APIM.scala 44:96]
  wire [11:0] _cmOutTmp_6_T_25 = io_cmIn_2 * mem_cmOutTmp_6_MPORT_2_data; // @[APIM.scala 46:22]
  wire [11:0] _cmOutTmp_6_T_27 = _cmOutTmp_6_T_17 + _cmOutTmp_6_T_25; // @[APIM.scala 45:96]
  wire [11:0] _cmOutTmp_6_T_35 = io_cmIn_3 * mem_cmOutTmp_6_MPORT_3_data; // @[APIM.scala 47:22]
  wire [11:0] _cmOutTmp_6_T_37 = _cmOutTmp_6_T_27 + _cmOutTmp_6_T_35; // @[APIM.scala 46:96]
  wire [11:0] _GEN_94 = io_cme ? _cmOutTmp_6_T_37 : 12'h0; // @[APIM.scala 42:18 43:19 49:19]
  wire [11:0] _cmOutTmp_7_T_7 = io_cmIn_0 * mem_cmOutTmp_7_MPORT_data; // @[APIM.scala 44:22]
  wire [11:0] _cmOutTmp_7_T_15 = io_cmIn_1 * mem_cmOutTmp_7_MPORT_1_data; // @[APIM.scala 45:22]
  wire [11:0] _cmOutTmp_7_T_17 = _cmOutTmp_7_T_7 + _cmOutTmp_7_T_15; // @[APIM.scala 44:96]
  wire [11:0] _cmOutTmp_7_T_25 = io_cmIn_2 * mem_cmOutTmp_7_MPORT_2_data; // @[APIM.scala 46:22]
  wire [11:0] _cmOutTmp_7_T_27 = _cmOutTmp_7_T_17 + _cmOutTmp_7_T_25; // @[APIM.scala 45:96]
  wire [11:0] _cmOutTmp_7_T_35 = io_cmIn_3 * mem_cmOutTmp_7_MPORT_3_data; // @[APIM.scala 47:22]
  wire [11:0] _cmOutTmp_7_T_37 = _cmOutTmp_7_T_27 + _cmOutTmp_7_T_35; // @[APIM.scala 46:96]
  wire [11:0] _GEN_107 = io_cme ? _cmOutTmp_7_T_37 : 12'h0; // @[APIM.scala 42:18 43:19 49:19]
  assign mem_cmOutTmp_0_MPORT_en = mem_cmOutTmp_0_MPORT_en_pipe_0;
  assign mem_cmOutTmp_0_MPORT_addr = mem_cmOutTmp_0_MPORT_addr_pipe_0;
  assign mem_cmOutTmp_0_MPORT_data = mem[mem_cmOutTmp_0_MPORT_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_0_MPORT_1_en = mem_cmOutTmp_0_MPORT_1_en_pipe_0;
  assign mem_cmOutTmp_0_MPORT_1_addr = mem_cmOutTmp_0_MPORT_1_addr_pipe_0;
  assign mem_cmOutTmp_0_MPORT_1_data = mem[mem_cmOutTmp_0_MPORT_1_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_0_MPORT_2_en = mem_cmOutTmp_0_MPORT_2_en_pipe_0;
  assign mem_cmOutTmp_0_MPORT_2_addr = mem_cmOutTmp_0_MPORT_2_addr_pipe_0;
  assign mem_cmOutTmp_0_MPORT_2_data = mem[mem_cmOutTmp_0_MPORT_2_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_0_MPORT_3_en = mem_cmOutTmp_0_MPORT_3_en_pipe_0;
  assign mem_cmOutTmp_0_MPORT_3_addr = mem_cmOutTmp_0_MPORT_3_addr_pipe_0;
  assign mem_cmOutTmp_0_MPORT_3_data = mem[mem_cmOutTmp_0_MPORT_3_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_1_MPORT_en = mem_cmOutTmp_1_MPORT_en_pipe_0;
  assign mem_cmOutTmp_1_MPORT_addr = mem_cmOutTmp_1_MPORT_addr_pipe_0;
  assign mem_cmOutTmp_1_MPORT_data = mem[mem_cmOutTmp_1_MPORT_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_1_MPORT_1_en = mem_cmOutTmp_1_MPORT_1_en_pipe_0;
  assign mem_cmOutTmp_1_MPORT_1_addr = mem_cmOutTmp_1_MPORT_1_addr_pipe_0;
  assign mem_cmOutTmp_1_MPORT_1_data = mem[mem_cmOutTmp_1_MPORT_1_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_1_MPORT_2_en = mem_cmOutTmp_1_MPORT_2_en_pipe_0;
  assign mem_cmOutTmp_1_MPORT_2_addr = mem_cmOutTmp_1_MPORT_2_addr_pipe_0;
  assign mem_cmOutTmp_1_MPORT_2_data = mem[mem_cmOutTmp_1_MPORT_2_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_1_MPORT_3_en = mem_cmOutTmp_1_MPORT_3_en_pipe_0;
  assign mem_cmOutTmp_1_MPORT_3_addr = mem_cmOutTmp_1_MPORT_3_addr_pipe_0;
  assign mem_cmOutTmp_1_MPORT_3_data = mem[mem_cmOutTmp_1_MPORT_3_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_2_MPORT_en = mem_cmOutTmp_2_MPORT_en_pipe_0;
  assign mem_cmOutTmp_2_MPORT_addr = mem_cmOutTmp_2_MPORT_addr_pipe_0;
  assign mem_cmOutTmp_2_MPORT_data = mem[mem_cmOutTmp_2_MPORT_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_2_MPORT_1_en = mem_cmOutTmp_2_MPORT_1_en_pipe_0;
  assign mem_cmOutTmp_2_MPORT_1_addr = mem_cmOutTmp_2_MPORT_1_addr_pipe_0;
  assign mem_cmOutTmp_2_MPORT_1_data = mem[mem_cmOutTmp_2_MPORT_1_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_2_MPORT_2_en = mem_cmOutTmp_2_MPORT_2_en_pipe_0;
  assign mem_cmOutTmp_2_MPORT_2_addr = mem_cmOutTmp_2_MPORT_2_addr_pipe_0;
  assign mem_cmOutTmp_2_MPORT_2_data = mem[mem_cmOutTmp_2_MPORT_2_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_2_MPORT_3_en = mem_cmOutTmp_2_MPORT_3_en_pipe_0;
  assign mem_cmOutTmp_2_MPORT_3_addr = mem_cmOutTmp_2_MPORT_3_addr_pipe_0;
  assign mem_cmOutTmp_2_MPORT_3_data = mem[mem_cmOutTmp_2_MPORT_3_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_3_MPORT_en = mem_cmOutTmp_3_MPORT_en_pipe_0;
  assign mem_cmOutTmp_3_MPORT_addr = mem_cmOutTmp_3_MPORT_addr_pipe_0;
  assign mem_cmOutTmp_3_MPORT_data = mem[mem_cmOutTmp_3_MPORT_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_3_MPORT_1_en = mem_cmOutTmp_3_MPORT_1_en_pipe_0;
  assign mem_cmOutTmp_3_MPORT_1_addr = mem_cmOutTmp_3_MPORT_1_addr_pipe_0;
  assign mem_cmOutTmp_3_MPORT_1_data = mem[mem_cmOutTmp_3_MPORT_1_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_3_MPORT_2_en = mem_cmOutTmp_3_MPORT_2_en_pipe_0;
  assign mem_cmOutTmp_3_MPORT_2_addr = mem_cmOutTmp_3_MPORT_2_addr_pipe_0;
  assign mem_cmOutTmp_3_MPORT_2_data = mem[mem_cmOutTmp_3_MPORT_2_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_3_MPORT_3_en = mem_cmOutTmp_3_MPORT_3_en_pipe_0;
  assign mem_cmOutTmp_3_MPORT_3_addr = mem_cmOutTmp_3_MPORT_3_addr_pipe_0;
  assign mem_cmOutTmp_3_MPORT_3_data = mem[mem_cmOutTmp_3_MPORT_3_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_4_MPORT_en = mem_cmOutTmp_4_MPORT_en_pipe_0;
  assign mem_cmOutTmp_4_MPORT_addr = mem_cmOutTmp_4_MPORT_addr_pipe_0;
  assign mem_cmOutTmp_4_MPORT_data = mem[mem_cmOutTmp_4_MPORT_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_4_MPORT_1_en = mem_cmOutTmp_4_MPORT_1_en_pipe_0;
  assign mem_cmOutTmp_4_MPORT_1_addr = mem_cmOutTmp_4_MPORT_1_addr_pipe_0;
  assign mem_cmOutTmp_4_MPORT_1_data = mem[mem_cmOutTmp_4_MPORT_1_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_4_MPORT_2_en = mem_cmOutTmp_4_MPORT_2_en_pipe_0;
  assign mem_cmOutTmp_4_MPORT_2_addr = mem_cmOutTmp_4_MPORT_2_addr_pipe_0;
  assign mem_cmOutTmp_4_MPORT_2_data = mem[mem_cmOutTmp_4_MPORT_2_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_4_MPORT_3_en = mem_cmOutTmp_4_MPORT_3_en_pipe_0;
  assign mem_cmOutTmp_4_MPORT_3_addr = mem_cmOutTmp_4_MPORT_3_addr_pipe_0;
  assign mem_cmOutTmp_4_MPORT_3_data = mem[mem_cmOutTmp_4_MPORT_3_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_5_MPORT_en = mem_cmOutTmp_5_MPORT_en_pipe_0;
  assign mem_cmOutTmp_5_MPORT_addr = mem_cmOutTmp_5_MPORT_addr_pipe_0;
  assign mem_cmOutTmp_5_MPORT_data = mem[mem_cmOutTmp_5_MPORT_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_5_MPORT_1_en = mem_cmOutTmp_5_MPORT_1_en_pipe_0;
  assign mem_cmOutTmp_5_MPORT_1_addr = mem_cmOutTmp_5_MPORT_1_addr_pipe_0;
  assign mem_cmOutTmp_5_MPORT_1_data = mem[mem_cmOutTmp_5_MPORT_1_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_5_MPORT_2_en = mem_cmOutTmp_5_MPORT_2_en_pipe_0;
  assign mem_cmOutTmp_5_MPORT_2_addr = mem_cmOutTmp_5_MPORT_2_addr_pipe_0;
  assign mem_cmOutTmp_5_MPORT_2_data = mem[mem_cmOutTmp_5_MPORT_2_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_5_MPORT_3_en = mem_cmOutTmp_5_MPORT_3_en_pipe_0;
  assign mem_cmOutTmp_5_MPORT_3_addr = mem_cmOutTmp_5_MPORT_3_addr_pipe_0;
  assign mem_cmOutTmp_5_MPORT_3_data = mem[mem_cmOutTmp_5_MPORT_3_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_6_MPORT_en = mem_cmOutTmp_6_MPORT_en_pipe_0;
  assign mem_cmOutTmp_6_MPORT_addr = mem_cmOutTmp_6_MPORT_addr_pipe_0;
  assign mem_cmOutTmp_6_MPORT_data = mem[mem_cmOutTmp_6_MPORT_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_6_MPORT_1_en = mem_cmOutTmp_6_MPORT_1_en_pipe_0;
  assign mem_cmOutTmp_6_MPORT_1_addr = mem_cmOutTmp_6_MPORT_1_addr_pipe_0;
  assign mem_cmOutTmp_6_MPORT_1_data = mem[mem_cmOutTmp_6_MPORT_1_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_6_MPORT_2_en = mem_cmOutTmp_6_MPORT_2_en_pipe_0;
  assign mem_cmOutTmp_6_MPORT_2_addr = mem_cmOutTmp_6_MPORT_2_addr_pipe_0;
  assign mem_cmOutTmp_6_MPORT_2_data = mem[mem_cmOutTmp_6_MPORT_2_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_6_MPORT_3_en = mem_cmOutTmp_6_MPORT_3_en_pipe_0;
  assign mem_cmOutTmp_6_MPORT_3_addr = mem_cmOutTmp_6_MPORT_3_addr_pipe_0;
  assign mem_cmOutTmp_6_MPORT_3_data = mem[mem_cmOutTmp_6_MPORT_3_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_7_MPORT_en = mem_cmOutTmp_7_MPORT_en_pipe_0;
  assign mem_cmOutTmp_7_MPORT_addr = mem_cmOutTmp_7_MPORT_addr_pipe_0;
  assign mem_cmOutTmp_7_MPORT_data = mem[mem_cmOutTmp_7_MPORT_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_7_MPORT_1_en = mem_cmOutTmp_7_MPORT_1_en_pipe_0;
  assign mem_cmOutTmp_7_MPORT_1_addr = mem_cmOutTmp_7_MPORT_1_addr_pipe_0;
  assign mem_cmOutTmp_7_MPORT_1_data = mem[mem_cmOutTmp_7_MPORT_1_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_7_MPORT_2_en = mem_cmOutTmp_7_MPORT_2_en_pipe_0;
  assign mem_cmOutTmp_7_MPORT_2_addr = mem_cmOutTmp_7_MPORT_2_addr_pipe_0;
  assign mem_cmOutTmp_7_MPORT_2_data = mem[mem_cmOutTmp_7_MPORT_2_addr]; // @[APIM.scala 37:24]
  assign mem_cmOutTmp_7_MPORT_3_en = mem_cmOutTmp_7_MPORT_3_en_pipe_0;
  assign mem_cmOutTmp_7_MPORT_3_addr = mem_cmOutTmp_7_MPORT_3_addr_pipe_0;
  assign mem_cmOutTmp_7_MPORT_3_data = mem[mem_cmOutTmp_7_MPORT_3_addr]; // @[APIM.scala 37:24]
  assign mem_io_q_MPORT_en = mem_io_q_MPORT_en_pipe_0;
  assign mem_io_q_MPORT_addr = mem_io_q_MPORT_addr_pipe_0;
  assign mem_io_q_MPORT_data = mem[mem_io_q_MPORT_addr]; // @[APIM.scala 37:24]
  assign mem_MPORT_data = io_d;
  assign mem_MPORT_addr = io_addr;
  assign mem_MPORT_mask = 1'h1;
  assign mem_MPORT_en = io_we;
  assign io_q = io_we ? io_d : mem_io_q_MPORT_data; // @[APIM.scala 59:16 61:10 63:10]
  assign io_cmOut_0 = cmOutTmp_0[11:8]; // @[APIM.scala 54:17]
  assign io_cmOut_1 = cmOutTmp_1[11:8]; // @[APIM.scala 54:17]
  assign io_cmOut_2 = cmOutTmp_2[11:8]; // @[APIM.scala 54:17]
  assign io_cmOut_3 = cmOutTmp_3[11:8]; // @[APIM.scala 54:17]
  assign io_cmOut_4 = cmOutTmp_4[11:8]; // @[APIM.scala 54:17]
  assign io_cmOut_5 = cmOutTmp_5[11:8]; // @[APIM.scala 54:17]
  assign io_cmOut_6 = cmOutTmp_6[11:8]; // @[APIM.scala 54:17]
  assign io_cmOut_7 = cmOutTmp_7[11:8]; // @[APIM.scala 54:17]
  always @(posedge clock) begin
    if (mem_MPORT_en & mem_MPORT_mask) begin
      mem[mem_MPORT_addr] <= mem_MPORT_data; // @[APIM.scala 37:24]
    end
    mem_cmOutTmp_0_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_0_MPORT_addr_pipe_0 <= io_addr & 10'h1c7;
    end
    mem_cmOutTmp_0_MPORT_1_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_0_MPORT_1_addr_pipe_0 <= 10'h100 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_0_MPORT_2_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_0_MPORT_2_addr_pipe_0 <= 10'h200 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_0_MPORT_3_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_0_MPORT_3_addr_pipe_0 <= 10'h300 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_1_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_1_MPORT_addr_pipe_0 <= 10'h4 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_1_MPORT_1_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_1_MPORT_1_addr_pipe_0 <= 10'h104 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_1_MPORT_2_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_1_MPORT_2_addr_pipe_0 <= 10'h204 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_1_MPORT_3_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_1_MPORT_3_addr_pipe_0 <= 10'h304 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_2_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_2_MPORT_addr_pipe_0 <= 10'h8 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_2_MPORT_1_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_2_MPORT_1_addr_pipe_0 <= 10'h108 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_2_MPORT_2_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_2_MPORT_2_addr_pipe_0 <= 10'h208 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_2_MPORT_3_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_2_MPORT_3_addr_pipe_0 <= 10'h308 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_3_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_3_MPORT_addr_pipe_0 <= 10'hc | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_3_MPORT_1_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_3_MPORT_1_addr_pipe_0 <= 10'h10c | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_3_MPORT_2_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_3_MPORT_2_addr_pipe_0 <= 10'h20c | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_3_MPORT_3_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_3_MPORT_3_addr_pipe_0 <= 10'h30c | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_4_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_4_MPORT_addr_pipe_0 <= 10'h10 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_4_MPORT_1_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_4_MPORT_1_addr_pipe_0 <= 10'h110 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_4_MPORT_2_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_4_MPORT_2_addr_pipe_0 <= 10'h210 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_4_MPORT_3_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_4_MPORT_3_addr_pipe_0 <= 10'h310 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_5_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_5_MPORT_addr_pipe_0 <= 10'h14 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_5_MPORT_1_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_5_MPORT_1_addr_pipe_0 <= 10'h114 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_5_MPORT_2_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_5_MPORT_2_addr_pipe_0 <= 10'h214 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_5_MPORT_3_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_5_MPORT_3_addr_pipe_0 <= 10'h314 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_6_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_6_MPORT_addr_pipe_0 <= 10'h18 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_6_MPORT_1_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_6_MPORT_1_addr_pipe_0 <= 10'h118 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_6_MPORT_2_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_6_MPORT_2_addr_pipe_0 <= 10'h218 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_6_MPORT_3_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_6_MPORT_3_addr_pipe_0 <= 10'h318 | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_7_MPORT_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_7_MPORT_addr_pipe_0 <= 10'h1c | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_7_MPORT_1_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_7_MPORT_1_addr_pipe_0 <= 10'h11c | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_7_MPORT_2_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_7_MPORT_2_addr_pipe_0 <= 10'h21c | _cmOutTmp_0_T_3;
    end
    mem_cmOutTmp_7_MPORT_3_en_pipe_0 <= io_cme;
    if (io_cme) begin
      mem_cmOutTmp_7_MPORT_3_addr_pipe_0 <= 10'h31c | _cmOutTmp_0_T_3;
    end
    if (io_we) begin
      mem_io_q_MPORT_en_pipe_0 <= 1'h0;
    end else begin
      mem_io_q_MPORT_en_pipe_0 <= 1'h1;
    end
    if (io_we ? 1'h0 : 1'h1) begin
      mem_io_q_MPORT_addr_pipe_0 <= io_addr;
    end
    cmOutTmp_0 <= {{20'd0}, _GEN_16};
    cmOutTmp_1 <= {{20'd0}, _GEN_29};
    cmOutTmp_2 <= {{20'd0}, _GEN_42};
    cmOutTmp_3 <= {{20'd0}, _GEN_55};
    cmOutTmp_4 <= {{20'd0}, _GEN_68};
    cmOutTmp_5 <= {{20'd0}, _GEN_81};
    cmOutTmp_6 <= {{20'd0}, _GEN_94};
    cmOutTmp_7 <= {{20'd0}, _GEN_107};
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_MEM_INIT
  _RAND_0 = {1{`RANDOM}};
  for (initvar = 0; initvar < 1024; initvar = initvar+1)
    mem[initvar] = _RAND_0[7:0];
`endif // RANDOMIZE_MEM_INIT
`ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  mem_cmOutTmp_0_MPORT_en_pipe_0 = _RAND_1[0:0];
  _RAND_2 = {1{`RANDOM}};
  mem_cmOutTmp_0_MPORT_addr_pipe_0 = _RAND_2[9:0];
  _RAND_3 = {1{`RANDOM}};
  mem_cmOutTmp_0_MPORT_1_en_pipe_0 = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  mem_cmOutTmp_0_MPORT_1_addr_pipe_0 = _RAND_4[9:0];
  _RAND_5 = {1{`RANDOM}};
  mem_cmOutTmp_0_MPORT_2_en_pipe_0 = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  mem_cmOutTmp_0_MPORT_2_addr_pipe_0 = _RAND_6[9:0];
  _RAND_7 = {1{`RANDOM}};
  mem_cmOutTmp_0_MPORT_3_en_pipe_0 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  mem_cmOutTmp_0_MPORT_3_addr_pipe_0 = _RAND_8[9:0];
  _RAND_9 = {1{`RANDOM}};
  mem_cmOutTmp_1_MPORT_en_pipe_0 = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  mem_cmOutTmp_1_MPORT_addr_pipe_0 = _RAND_10[9:0];
  _RAND_11 = {1{`RANDOM}};
  mem_cmOutTmp_1_MPORT_1_en_pipe_0 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  mem_cmOutTmp_1_MPORT_1_addr_pipe_0 = _RAND_12[9:0];
  _RAND_13 = {1{`RANDOM}};
  mem_cmOutTmp_1_MPORT_2_en_pipe_0 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  mem_cmOutTmp_1_MPORT_2_addr_pipe_0 = _RAND_14[9:0];
  _RAND_15 = {1{`RANDOM}};
  mem_cmOutTmp_1_MPORT_3_en_pipe_0 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  mem_cmOutTmp_1_MPORT_3_addr_pipe_0 = _RAND_16[9:0];
  _RAND_17 = {1{`RANDOM}};
  mem_cmOutTmp_2_MPORT_en_pipe_0 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  mem_cmOutTmp_2_MPORT_addr_pipe_0 = _RAND_18[9:0];
  _RAND_19 = {1{`RANDOM}};
  mem_cmOutTmp_2_MPORT_1_en_pipe_0 = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  mem_cmOutTmp_2_MPORT_1_addr_pipe_0 = _RAND_20[9:0];
  _RAND_21 = {1{`RANDOM}};
  mem_cmOutTmp_2_MPORT_2_en_pipe_0 = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  mem_cmOutTmp_2_MPORT_2_addr_pipe_0 = _RAND_22[9:0];
  _RAND_23 = {1{`RANDOM}};
  mem_cmOutTmp_2_MPORT_3_en_pipe_0 = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  mem_cmOutTmp_2_MPORT_3_addr_pipe_0 = _RAND_24[9:0];
  _RAND_25 = {1{`RANDOM}};
  mem_cmOutTmp_3_MPORT_en_pipe_0 = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  mem_cmOutTmp_3_MPORT_addr_pipe_0 = _RAND_26[9:0];
  _RAND_27 = {1{`RANDOM}};
  mem_cmOutTmp_3_MPORT_1_en_pipe_0 = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  mem_cmOutTmp_3_MPORT_1_addr_pipe_0 = _RAND_28[9:0];
  _RAND_29 = {1{`RANDOM}};
  mem_cmOutTmp_3_MPORT_2_en_pipe_0 = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  mem_cmOutTmp_3_MPORT_2_addr_pipe_0 = _RAND_30[9:0];
  _RAND_31 = {1{`RANDOM}};
  mem_cmOutTmp_3_MPORT_3_en_pipe_0 = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  mem_cmOutTmp_3_MPORT_3_addr_pipe_0 = _RAND_32[9:0];
  _RAND_33 = {1{`RANDOM}};
  mem_cmOutTmp_4_MPORT_en_pipe_0 = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  mem_cmOutTmp_4_MPORT_addr_pipe_0 = _RAND_34[9:0];
  _RAND_35 = {1{`RANDOM}};
  mem_cmOutTmp_4_MPORT_1_en_pipe_0 = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  mem_cmOutTmp_4_MPORT_1_addr_pipe_0 = _RAND_36[9:0];
  _RAND_37 = {1{`RANDOM}};
  mem_cmOutTmp_4_MPORT_2_en_pipe_0 = _RAND_37[0:0];
  _RAND_38 = {1{`RANDOM}};
  mem_cmOutTmp_4_MPORT_2_addr_pipe_0 = _RAND_38[9:0];
  _RAND_39 = {1{`RANDOM}};
  mem_cmOutTmp_4_MPORT_3_en_pipe_0 = _RAND_39[0:0];
  _RAND_40 = {1{`RANDOM}};
  mem_cmOutTmp_4_MPORT_3_addr_pipe_0 = _RAND_40[9:0];
  _RAND_41 = {1{`RANDOM}};
  mem_cmOutTmp_5_MPORT_en_pipe_0 = _RAND_41[0:0];
  _RAND_42 = {1{`RANDOM}};
  mem_cmOutTmp_5_MPORT_addr_pipe_0 = _RAND_42[9:0];
  _RAND_43 = {1{`RANDOM}};
  mem_cmOutTmp_5_MPORT_1_en_pipe_0 = _RAND_43[0:0];
  _RAND_44 = {1{`RANDOM}};
  mem_cmOutTmp_5_MPORT_1_addr_pipe_0 = _RAND_44[9:0];
  _RAND_45 = {1{`RANDOM}};
  mem_cmOutTmp_5_MPORT_2_en_pipe_0 = _RAND_45[0:0];
  _RAND_46 = {1{`RANDOM}};
  mem_cmOutTmp_5_MPORT_2_addr_pipe_0 = _RAND_46[9:0];
  _RAND_47 = {1{`RANDOM}};
  mem_cmOutTmp_5_MPORT_3_en_pipe_0 = _RAND_47[0:0];
  _RAND_48 = {1{`RANDOM}};
  mem_cmOutTmp_5_MPORT_3_addr_pipe_0 = _RAND_48[9:0];
  _RAND_49 = {1{`RANDOM}};
  mem_cmOutTmp_6_MPORT_en_pipe_0 = _RAND_49[0:0];
  _RAND_50 = {1{`RANDOM}};
  mem_cmOutTmp_6_MPORT_addr_pipe_0 = _RAND_50[9:0];
  _RAND_51 = {1{`RANDOM}};
  mem_cmOutTmp_6_MPORT_1_en_pipe_0 = _RAND_51[0:0];
  _RAND_52 = {1{`RANDOM}};
  mem_cmOutTmp_6_MPORT_1_addr_pipe_0 = _RAND_52[9:0];
  _RAND_53 = {1{`RANDOM}};
  mem_cmOutTmp_6_MPORT_2_en_pipe_0 = _RAND_53[0:0];
  _RAND_54 = {1{`RANDOM}};
  mem_cmOutTmp_6_MPORT_2_addr_pipe_0 = _RAND_54[9:0];
  _RAND_55 = {1{`RANDOM}};
  mem_cmOutTmp_6_MPORT_3_en_pipe_0 = _RAND_55[0:0];
  _RAND_56 = {1{`RANDOM}};
  mem_cmOutTmp_6_MPORT_3_addr_pipe_0 = _RAND_56[9:0];
  _RAND_57 = {1{`RANDOM}};
  mem_cmOutTmp_7_MPORT_en_pipe_0 = _RAND_57[0:0];
  _RAND_58 = {1{`RANDOM}};
  mem_cmOutTmp_7_MPORT_addr_pipe_0 = _RAND_58[9:0];
  _RAND_59 = {1{`RANDOM}};
  mem_cmOutTmp_7_MPORT_1_en_pipe_0 = _RAND_59[0:0];
  _RAND_60 = {1{`RANDOM}};
  mem_cmOutTmp_7_MPORT_1_addr_pipe_0 = _RAND_60[9:0];
  _RAND_61 = {1{`RANDOM}};
  mem_cmOutTmp_7_MPORT_2_en_pipe_0 = _RAND_61[0:0];
  _RAND_62 = {1{`RANDOM}};
  mem_cmOutTmp_7_MPORT_2_addr_pipe_0 = _RAND_62[9:0];
  _RAND_63 = {1{`RANDOM}};
  mem_cmOutTmp_7_MPORT_3_en_pipe_0 = _RAND_63[0:0];
  _RAND_64 = {1{`RANDOM}};
  mem_cmOutTmp_7_MPORT_3_addr_pipe_0 = _RAND_64[9:0];
  _RAND_65 = {1{`RANDOM}};
  mem_io_q_MPORT_en_pipe_0 = _RAND_65[0:0];
  _RAND_66 = {1{`RANDOM}};
  mem_io_q_MPORT_addr_pipe_0 = _RAND_66[9:0];
  _RAND_67 = {1{`RANDOM}};
  cmOutTmp_0 = _RAND_67[31:0];
  _RAND_68 = {1{`RANDOM}};
  cmOutTmp_1 = _RAND_68[31:0];
  _RAND_69 = {1{`RANDOM}};
  cmOutTmp_2 = _RAND_69[31:0];
  _RAND_70 = {1{`RANDOM}};
  cmOutTmp_3 = _RAND_70[31:0];
  _RAND_71 = {1{`RANDOM}};
  cmOutTmp_4 = _RAND_71[31:0];
  _RAND_72 = {1{`RANDOM}};
  cmOutTmp_5 = _RAND_72[31:0];
  _RAND_73 = {1{`RANDOM}};
  cmOutTmp_6 = _RAND_73[31:0];
  _RAND_74 = {1{`RANDOM}};
  cmOutTmp_7 = _RAND_74[31:0];
`endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
