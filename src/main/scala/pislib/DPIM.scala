/*
 * Copyright: 2022, Bonan Yan's Research Group
 * File 			    :   DPIM.scala
 * Updated Time 	:   2022/12/16 18:15:29
 * Author 			  :   Bonan Yan's Group (bonanyan@pku.edu.cn)
 * Description 		:	  This is a behavioral model for Digital PIM
 * Reference      :
 *      B. Yan et al., "A 1.041-Mb/mm2 27.38-TOPS/W Signed-INT8 Dynamic-Logic-Based ADC-less
 *      SRAM Compute-in-Memory Macro in 28nm with Reconfigurable Bitwise Operation for AI and Embedded Applications,"
 *      2022 IEEE International Solid- State Circuits Conference (ISSCC), 2022, pp. 188-190,
 *      doi: 10.1109/ISSCC42614.2022.9731545.
 */

package pislib

import chisel3._
import chisel3.util._
import chisel3.stage.ChiselStage

class DPIM(wordLen: Int, addrLen: Int) extends Module {
  val io = IO(new Bundle {
    val addr = Input(UInt(addrLen.W))
    val d = Input(UInt(wordLen.W))
    val q = Output(UInt(wordLen.W))
    val we = Input(Bool())
    val cme = Input(Bool())
    val cmIn = Input(Vec(32, UInt(32.W)))
    val cmOut = Output(Vec(32, UInt(32.W)))
    val func = Input(UInt(2.W))
  })


  val c = Wire(Vec(32, UInt(32.W)))
  val a = Wire(Vec(32, UInt(32.W)))
  val b = Wire(Vec(32, UInt(32.W)))
  val mem = SyncReadMem(1<<addrLen, UInt(wordLen.W))
  //  define cmOutTmp
  when(io.cme) {
    for (i <- 0 until 32) {
      //      io.cmOut(i) := io.cmIn(i) ^ (mem.read((io.addr & "b00000_1111_111".U(12.W)) | (i << 7).asUInt(12.W))) //for debug, only one function
      a(i) := io.cmIn(i)
      b(i) := mem.read((io.addr & "b00000_1111_111".U(12.W)) | (i << 7).asUInt(12.W))
      when(io.func === 0.U(2.W)) {
        c(i) := a(i) * b(i) //pim analog circuits, no overflow protection
      }.elsewhen(io.func === 1.U(2.W)) {
        c(i) := a(i) & b(i)
      }.elsewhen(io.func === 2.U(2.W)) {
        c(i) := a(i) | b(i)
      }otherwise {
        c(i) := a(i) ^ b(i)
      }
    }
  } otherwise {
    for (i <- 0 until 32) {
      a(i) := 0.U
      b(i) := 0.U
      c(i) := 0.U
    }
  }
  for (i <- 0 until 32) {
    io.cmOut(i) := c(i)
  }



  //  define io.q
  when (io.we) {
    mem.write(io.addr, io.d)
    io.q := io.d //write-through
  } otherwise {
    io.q := mem.read(io.addr)
  }

} //end of module

//verilog generator
object VerilogGen_DPIM extends App {
  (new ChiselStage).emitVerilog(
    new DPIM(32,12),
    Array("--target-dir", "generated/")
  )
}



