/*
 * Copyright: 2022, Bonan Yan's Research Group
 * File 			    :   Mem.scala
 * Updated Time 	:   2022/12/16 18:15:29
 * Author 			  :   Bonan Yan's Group (bonanyan@pku.edu.cn)
 * Description 		:	  memory unit behavioral model
 */

package pislib

import chisel3._
import chisel3.util._
import chisel3.stage.ChiselStage

class Memory(wordLen: Int, addrLen: Int) extends Module {
  /* Parameters:
  *  wordLen: bitwise length of each word/entry
  *  addrLen: bitwise length of memory address
  * */
  val io = IO(new Bundle{
    val addr = Input(UInt(10.W))
    val d = Input(UInt(wordLen.W))
    val q = Output(UInt(wordLen.W))
    val we = Input(Bool())
  })

  val mem = SyncReadMem(1<<addrLen, UInt(wordLen.W))

  io.q := mem.read(io.addr, ~io.we)
  when (io.we) {
    mem.write(io.addr, io.d)
  }
}

//verilog generator
object VerilogGen_Mem extends App {
  (new ChiselStage).emitVerilog(
    new Memory(32,10),
    Array("--target-dir", "generated/")
  )
}


