/*
 * Copyright: 2022, Bonan Yan's Research Group
 * File 			    :   MemoryTester1.scala
 * Updated Time 	:   2022/12/17 00:27:47
 * Author 			  :   Bonan Yan's Group (bonanyan@pku.edu.cn)
 * Description 		:	  Test DPIM
 */


package pislib

import chisel3._
import chisel3.tester._
import org.scalatest._

class APIMTester extends FlatSpec with ChiselScalatestTester with Matchers {
  behavior of "APIM"
  it should "do something" in {
    test(new APIM(8,10,6,4,8)) { c =>

      val DATA_WIDTH = 8
      val ADD_WIDTH = 10
      val ADC_PRECISION = 6
      val CIM_INPUT_PRECISION = 4
      val CIM_OUTPUT_PARALLELISM = 8


      //    Test memory function
      c.io.d.poke("hcd".U)
      c.io.addr.poke(0.U)
      c.io.we.poke(true.B)
      c.io.cme.poke(false.B)
      c.clock.step(1)

      c.io.d.poke("hab".U)
      c.io.addr.poke(2.U)
      c.io.we.poke(true.B)
      c.io.cme.poke(false.B)
      c.clock.step(1)

      c.io.d.poke("hfa".U)
      c.io.addr.poke(0.U)
      c.io.we.poke(false.B)
      c.io.cme.poke(false.B)
      c.clock.step(1)
      c.io.q.expect("hcd".U(DATA_WIDTH.W))

      c.io.d.poke("h21".U)
      c.io.addr.poke(2.U)
      c.io.we.poke(false.B)
      c.io.cme.poke(false.B)
      c.clock.step(1)
      c.io.q.expect("hab".U(DATA_WIDTH.W))

      println(">>Round 1 Test Memory Function Successfully Tested.<<<")

      val N = 1024
      val rand = new scala.util.Random
      var data = new Array[Int](1024)
      for(a <- 0 until N) {
        data(a) = rand.nextInt().abs % (1<< DATA_WIDTH)
        c.io.d.poke(data(a).asUInt)
        c.io.addr.poke(a.asUInt)
        c.io.we.poke(true.B)
        c.io.cme.poke(false.B)
        c.clock.step(1)
      }
      for (a <- 0 until N) {
        if((a % 128) === 0) {
          println(">>>> " + a.toString + " /1024 words done...")
        }
        c.io.d.poke(0.U)
        c.io.addr.poke(a.asUInt)
        c.io.we.poke(false.B)
        c.io.cme.poke(false.B)
        c.clock.step(1)
        c.io.q.expect(data(a).asUInt)
        for (i <- 0 until 8) {
          c.io.cmOut(i).expect(0.U)
        }
      }
      println(">>Round 2: Exhaustive Memory Mode Test is Successful<<<")

      //    test CIM function
      var din = new Array[Int](4)
      var dout = new Array[Int](CIM_OUTPUT_PARALLELISM)
      for(i<-0 until 4) {
        din(i) = (rand.nextInt().abs) % (1<<(CIM_INPUT_PRECISION-1)) // CIM_INPUT_PRECISION = 4bit, here give a smaller number to avoid overflow
      }
      for (a <- 0 until 1024) {
        c.io.d.poke(0.U)
        c.io.addr.poke(a.asUInt)
        c.io.we.poke(false.B)
        c.io.cme.poke(true.B)
        for(i <- 0 until 4) {
          c.io.cmIn(i).poke(din(i).asUInt)
        }
        c.clock.step(1)

        for (j <- 0 until CIM_OUTPUT_PARALLELISM) {
          dout(j) = din(0)*data((a&227)|(0<<8)|(j<<2)) +
                    din(1)*data((a&227)|(1<<8)|(j<<2)) +
                    din(2)*data((a&227)|(2<<8)|(j<<2)) +
                    din(3)*data((a&227)|(3<<8)|(j<<2))
//          dout(j) = (dout(j)>>8) % (1<<ADC_PRECISION)
          dout(j) = dout(j)>>8

          println("---\nTarget:: j= " + j.toString +
              ", dout= " + dout(j).toString +
              ", din=[" + din(0).toString + ","+ din(1).toString + ","+ din(2).toString + ","+ din(3).toString + "]" +
              ", data=[" + data((a&227)|(0<<8)|(j<<2)).toString + ","+ data((a&227)|(1<<8)|(j<<2)).toString + ","+ data((a&227)|(2<<8)|(j<<2)).toString + ","+ data((a&227)|(3<<8)|(j<<2)).toString +"]"
          )
          println("Hardware:: j= " + j.toString + ", dout= " + c.io.cmOut(j).peek().litValue)
        }
        for (j <- 0 until CIM_OUTPUT_PARALLELISM) {
          c.io.cmOut(j).expect(dout(j).asUInt)
        }
      }

      println(">>Round 3: DPIM Mode Test is Successful<<<")


    }
  }
}
