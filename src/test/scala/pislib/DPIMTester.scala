/*
 * Copyright: 2022, Bonan Yan's Research Group
 * File 			    :   MemoryTester1.scala
 * Updated Time 	:   2022/12/17 00:27:47
 * Author 			  :   Bonan Yan's Group (bonanyan@pku.edu.cn)
 * Description 		:	  Test DPIM
 */


package pislib

import chisel3._
import chisel3.tester._
import org.scalatest._

class DPIMTester extends FlatSpec with ChiselScalatestTester with Matchers {
  behavior of "DPIM"
  it should "do something" in {
    test(new DPIM(32,12)) { c =>
      //    Test memory function
      c.io.d.poke("hcdcd".U)
      c.io.addr.poke(0.U)
      c.io.we.poke(true.B)
      c.io.cme.poke(false.B)
      c.clock.step(1)

      c.io.d.poke("habab".U)
      c.io.addr.poke(2.U)
      c.io.we.poke(true.B)
      c.io.cme.poke(false.B)
      c.clock.step(1)

      c.io.d.poke("hfafa".U)
      c.io.addr.poke(0.U)
      c.io.we.poke(false.B)
      c.io.cme.poke(false.B)
      c.clock.step(1)
      c.io.q.expect("hcdcd".U(32.W))

      c.io.d.poke("h121".U)
      c.io.addr.poke(2.U)
      c.io.we.poke(false.B)
      c.io.cme.poke(false.B)
      c.clock.step(1)
      c.io.q.expect("habab".U(32.W))

      println(">>Round 1 Test Memory Function Successfully Tested.<<<")

      val N = 4096
      val rand = new scala.util.Random
      var data = new Array[Int](4096)
      for(a <- 0 until N) {
        data(a) = rand.nextInt().abs
        c.io.d.poke(data(a).asUInt)
        c.io.addr.poke(a.asUInt)
        c.io.we.poke(true.B)
        c.io.cme.poke(false.B)
        c.clock.step(1)
      }
      for (a <- 0 until N) {
        if((a % 512) === 0) {
          println(">>>> " + a.toString + " /4096 words done...")
        }
        c.io.d.poke(0.U)
        c.io.addr.poke(a.asUInt)
        c.io.we.poke(false.B)
        c.io.cme.poke(false.B)
        c.clock.step(1)
        c.io.q.expect(data(a).asUInt)
        for (i <- 0 until 32) {
          c.io.cmOut(i).expect(0.U)
        }

        //        println(c.io.q.peek().litValue)
        //        println(data(a))
      }
      println(">>Round 2: Exhaustive Memory Mode Test is Successful<<<")
      //
      //    test CIM function
      var din = new Array[Int](32)
      var dout = new Array[Int](32)
      for(i<-0 until 32) {
        din(i) = rand.nextInt().abs
      }
      for (a <- 0 until 2) {
        if(a==0) {
          c.io.func.poke(1.U(2.W)) //test & operation
        } else {
          c.io.func.poke(3.U(2.W)) //test ^ operation
        }
        c.io.d.poke(0.U)
        c.io.addr.poke(a.asUInt)
        c.io.we.poke(false.B)
        c.io.cme.poke(true.B)
        for(i <- 0 until 32) {
          c.io.cmIn(i).poke(din(i).asUInt)
        }
        c.clock.step(1)

        for (j <- 0 until 32) {
          if(a==0) {
            dout(j) = (data(j * 128 + a)) & (din(j))
          }
          else {
            dout(j) = (data(j * 128 + a)) ^ (din(j))
          }

          println("---\nTarget:: j= " + j.toString + ", data= " + data(j * 128 + a).toString +
              ", din= " + din(j).toString +
              ", dout= " + dout(j).toString )

          println("Hardware:: j= " + j.toString +
              ", data= " + data(j * 128 + a).toString +
              ", din= " + c.io.cmIn(j).peek().litValue +
              ", dout= " + c.io.cmOut(j).peek().litValue +
              ", func= " + c.io.func.peek().litValue
          )
          //          dout(j) = din(j)
        }
        for (i <- 0 until 32) {
          c.io.cmOut(i).expect(dout(i).asUInt)
        }
      }

      println(">>Round 3: DPIM Mode Test is Successful<<<")

    }
  }
}
